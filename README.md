# NT 6.x Fast Installer

The fastest way to install Windows ISOs or WIMs directly to a partition or USB drive from a running Windows system. Using this tool, you can install Windows without needing to extract or burn an ISO file. You can run this tool on an existing Windows system in the background, and continue using your PC while installation is in progress!

This tool can **install** Windows Vista and above, it can be **run** on ~~Windows 2000~~ Windows 7 and above. You can also run it from a WinPE system.

This is a fork of [fujianabc's NT 6.x Fast Installer](http://reboot.pro/topic/10126-nt-6x-fast-installer-install-win7-directly-to-usb-external-drive/), changes include:

- an optional file picker GUI for the ISO or WIM file to be installed (requires Windows 7 or above)
- automatically mount ISOs (requires Windows 8 or above)
- this fork can only be run on systems with Powershell (Windows 7 or above, or older system with the Powershell installed); mounting ISOs only works on Windows 8 and above.



PS:

1. If you install vista or 2008 from the original iso with this installer, the windows partition will occupy D:

2. After installing the OS on a USB hard drive, you should  open the write cache of the hard drive in device manager to speedup the system.

3. If you want to plug the usb OS to another PC, please run the command as administrator
  `\Windows\System32\sysprep\sysprep.exe /oobe /generalize /shutdown`
  After the shutting down the computer, you can plug the hard disk to another computer. 					 					

