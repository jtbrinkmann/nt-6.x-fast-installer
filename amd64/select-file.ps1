Add-Type -AssemblyName System.Windows.Forms
$f = new-object Windows.Forms.OpenFileDialog
$f.InitialDirectory = pwd
$f.Filter = "Windows ISO (*.iso); install.wim (*.wim)|*.iso;*.wim|All Files (*.*)|*.*"
[void]$f.ShowDialog($this)
if ($f.Multiselect) { $f.FileNames[0] } else { $f.FileName }
