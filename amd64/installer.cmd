@echo off
SET TP=%~dp0
SET TP=%TP:~0,-1%
cd /d "%TP%"
color 2f
chcp 437
mode con lines=40 cols=100
set title=                               Windows Vista/2008/7/2008 R2 Fast Installer
set updatetime=   ======================================== 2010.02.19 ===========================================


REM --------------------------------------------------------------------------------------
REM checking necessary files
REM --------------------------------------------------------------------------------------
for %%i in (imagex bcdboot bootsect reg) do (
if not exist %%i.exe if not exist %SystemRoot%\system32\%%i.exe (
echo %%i.exe is missing. Setup can't continue.
goto fail
)
)


REM --------------------------------------------------------------------------------------
REM start
REM --------------------------------------------------------------------------------------
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    This installer extracts the install file (install.wim) of  Windows Vista/2008/7/2008 R2 x86/x64
echo.   to hard disk, and then automatically creates the boot menu and modifies the bootsector. After
echo    reboot, your computer will directly go into the second stage of Windows installation.
echo.   
echo    Furthermore, you can directly install Windows to USB hard disk with this installer.
echo.
echo.
echo    Press Enter to continue, or input any letter to show the readme.
echo    If you want to exit, close this window....
SET Choice=
SET /P Choice=
IF "%Choice%" NEQ "" (
cls
type readme.txt
echo    Press any key to continue ...
pause>nul
cls
)
echo.
echo.
echo.


REM --------------------------------------------------------------------------------------
REM select install.wim
REM --------------------------------------------------------------------------------------
echo    Please select the path of install.wim.
echo    It is usually in the "sources" directory of your Windows CD.
start /wait wincmd load %TP%\getWim.ini
cd /d "%TP%"
call setWimpath.cmd
del setWimpath.cmd
IF '%Wimpath%'=='' goto fail
if not exist %Wimpath% (
echo.
echo    No wim file is selected.
goto fail
)
REM Get the path of the directory which contains install.wim 
for /f "delims=" %%a in (%Wimpath%) do set wimdir=%%~dpa
set wimdir=%wimdir:~0,-1%



REM --------------------------------------------------------------------------------------
REM show the contents of the selected wim file
REM --------------------------------------------------------------------------------------
cd /d "%TP%"
SET Wimindex=
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    Path of your Wim file: %Wimpath%
echo.
imagex /info %Wimpath% >wiminfo.txt


for /f "tokens=1,2 delims=:" %%i in (wiminfo.txt) do IF /I "%%i"=="Image Count" echo Total image number:%%j
echo.
echo Available image:
echo.
for /f "tokens=2,3 delims=<>=" %%i in (wiminfo.txt) do (
IF /I "%%i"=="IMAGE INDEX" set/p=%%j<nul
IF /I "%%i"=="NAME" echo . %%j
)
echo.

REM --------------------------------------------------------------------------------------
REM select the image number of the wim file
REM --------------------------------------------------------------------------------------
:indexnumber
SET Choice=
SET /P Choice=   Please input the index number (don't include quotation marks):
IF "%Choice%"=="" (
goto indexnumber
) else (
SET Choice=%Choice:~0,3%
)
IF "%Choice%"=="" goto indexnumber
SET errorindex=YES
for /f "tokens=2,3 delims=<>=" %%i in (wiminfo.txt) do IF /I "%%i"=="IMAGE INDEX" IF /I "%%j"==""%Choice%"" SET errorindex=NO
IF '%errorindex%'=='YES' (
echo    The index number is wrong, please input a correct number.
goto indexnumber
)
SET Wimindex=%Choice%
del wiminfo.txt >nul

REM Set language. Set the tag for vista/2008, in order to fix the system drive letter in the registry of Vista/2008.
set isvista=0
set lang=en-US
for /f "tokens=2,3 delims=<>=" %%i in ('imagex /info %Wimpath% %Choice%') do (
IF /I "%%i"=="LANGUAGE" set lang=%%j
IF /I "%%i"=="MAJOR" IF /I "%%j" NEQ "6" goto osdrive
IF /I "%%i"=="MINOR" IF /I "%%j"=="0" SET isvista=1
)


REM --------------------------------------------------------------------------------------
REM select system partition
REM --------------------------------------------------------------------------------------
:osdrive
cd /d "%TP%"
SET installpath=
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    Path of your Wim file: %Wimpath%
echo    The index number: %Wimindex%
echo.
echo.
echo    Please enter the drive letter of the partition
echo    where you want to install Windows:
echo.
SET Choice=
SET /P Choice=   Please enter the drive letter (e.g: C):
echo.
IF "%Choice%"=="" goto osdrive
SET Choice=%Choice:~0,1%:
vol %Choice%>nul 2>nul || (
echo    The partition %Choice% does not exist, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
echo Testing if the partition is writable >%Choice%\test.tst
if not exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
del %Choice%\test.tst>nul 2>nul
if exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
SET installpath=%Choice%


REM --------------------------------------------------------------------------------------
REM Verify if the partition is NTFS
REM --------------------------------------------------------------------------------------
md %installpath%\$mft >NUL
if not errorlevel 1 (
rd %installpath%\$mft >NUL
echo.
echo    %installpath% is not NTFS
echo    Please mannually convert or format this partition to NTFS first.
echo.
goto fail
)
cls

REM --------------------------------------------------------------------------------------
REM Verify the free size of the partition
REM --------------------------------------------------------------------------------------
set drvsize=
md %installpath%\$$$ >NUL
for /f "tokens=3" %%i in ('dir /-c %installpath%\') do set drvsize=%%i
rd %installpath%\$$$ >NUL
if %drvsize% LSS 1000000000 goto not_enough
set drvsize=%drvsize:~0,-9%
if %drvsize% LSS 10 (
goto not_enough
) else (
goto enough
)

:not_enough
echo. 
echo    The free size of %installpath% is less than 10GB, error may happen if you continue to install.   
echo. 
SET Choice=
SET /P Choice=    Input C to continue, or input others to select other partition:
IF /I not "%Choice%"=="C" (
goto osdrive
) else (
goto enough
)


REM --------------------------------------------------------------------------------------
REM Processing format
REM --------------------------------------------------------------------------------------
:enough
IF exist %installpath%\windows (
echo.
echo    %installpath% already contains another windows, format is needed.
echo.
) else (
goto bootdrive
)

:format_hd
format /? >nul 2>nul
if errorlevel 1 (
echo    The command "format" is missing in your current OS, installation process cannot continue. 
echo    Please format %installpath% by yourself, and then rerun this installer.   
goto fail
)
format /FS:NTFS /q /x %installpath%


REM --------------------------------------------------------------------------------------
REM select the boot partition
REM --------------------------------------------------------------------------------------
:bootdrive
cd /d "%TP%"
SET bootpath=
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    Path of your Wim file: %Wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo.
echo.
echo    Please enter the drive letter of your ACTIVE primary partition, normally it is C. 
echo    If there is an exception to you, please enter the correct letter.   
echo    For USB hard drive, the letter is the boot partition of your USB hard drive.
echo.
SET Choice=
SET /P Choice=   Please enter the drive letter (e.g. C):
echo.
IF "%Choice%"=="" goto bootdrive
SET Choice=%Choice:~0,1%:
vol %Choice%>nul 2>nul || (
echo    The partition %Choice% does not exist, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
echo Testing if the partition is writable >%Choice%\test.tst
if not exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
del %Choice%\test.tst>nul 2>nul
if exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
SET bootpath=%Choice%



REM --------------------------------------------------------------------------------------
REM Ask whether the target drive is a USB hard disk
REM --------------------------------------------------------------------------------------
:usbchoice
SET usbhd=
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    Path of your Wim file: %Wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo    Drive letter for boot partition: %bootpath%
echo.
echo.   
echo.
SET Choice=
SET /P Choice=    Is %installpath% a USB hard disk? (Y/N):
echo.
IF /I NOT "%Choice%"=="Y" IF /I NOT "%Choice%"=="N" goto usbchoice
IF /I "%Choice%"=="Y" (
SET usbhd=YES
) ELSE (
SET usbhd=NO
)


REM --------------------------------------------------------------------------------------
REM Ready to start the installation
REM --------------------------------------------------------------------------------------
cd /d "%TP%"
cls
echo.
echo %title%
echo.                   
echo %updatetime%
echo.
echo    Path of your Wim file: %Wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo    Drive letter for boot partition: %bootpath%
echo    USB hard disk or not: %usbhd%
echo.
echo.
echo    All informatition is collected! Press Enter to start installation ...   
IF %isvista% EQU 1 (
SET osletter=
pause >nul
goto :startinstall
)
echo    After the installation, the drive letter of the OS partition will be C:. If you want to 
echo    use other drive letter, please directly input the letter (e.g. D) and press Enter. 
echo    Attention! A and B is invalid.

REM --------------------------------------------------------------------------------------
REM get which letter the OS partition will occupy after the installation
REM --------------------------------------------------------------------------------------
:startletter
SET osletter=
SET Choice=
SET /P Choice=
IF "%Choice%"=="" goto startinstall
SET Choice=%Choice:~0,1%
IF /I %Choice% LEQ z IF /I %Choice% GEQ c (
set osletter=%Choice%
) ELSE (
echo    Input error, please reinput the letter or press Enter to start the intallation directly.   
goto startletter
)

REM --------------------------------------------------------------------------------------
REM extract the wim file
REM --------------------------------------------------------------------------------------
:startinstall
echo.
echo    Extracting the WIM file, it needs some time, please wait...
imagex /apply %Wimpath% %Wimindex% %installpath%
echo    The Wim file applied O.K.
echo.

REM --------------------------------------------------------------------------------------
REM Processing usb boot
REM --------------------------------------------------------------------------------------
cd /d "%TP%"
IF "%usbhd%"=="YES" (
echo.
echo    Processing the registry and usb boot watcher ...
echo.
reg load HKLM\sys %installpath%\WINDOWS\system32\config\system >nul 2>nul
if errorlevel 1 (
echo    Error occured when loading the registry
goto fail
)
REG IMPORT usb.reg >nul 2>nul
if errorlevel 1 (
echo    Error occured when modifying the registry
reg unload HKLM\sys >nul 2>nul
goto fail
)
echo.
reg unload HKLM\sys >nul 2>nul
if errorlevel 1 (
echo    Error occured when unloading the registry
goto fail
)
echo    Finish processing the registry
echo.
copy /y UsbBootWatcher.conf %installpath%\WINDOWS\system32 >NUL
if exist %installpath%\WINDOWS\SysWOW64 (
copy /y UsbBootWatcherx64.exe %installpath%\WINDOWS\system32\UsbBootWatcher.exe >NUL
) ELSE (
copy /y UsbBootWatcherx86.exe %installpath%\WINDOWS\system32\UsbBootWatcher.exe >NUL
)
echo.
REM Post-process tag for USB boot
md %installpath%\$usb
)

REM --------------------------------------------------------------------------------------
REM fix the drive letter of OS partition in the registry of Vista/2008
REM --------------------------------------------------------------------------------------
IF %isvista% EQU 1 (
echo    fixing the drive letter in the registry of Vista/2008
call fixletter.cmd /currentos:%installpath%
)


REM --------------------------------------------------------------------------------------
REM modify the OS partition letter
REM --------------------------------------------------------------------------------------
IF /I '%osletter%' NEQ '' (
echo    Processing the modification of the system drive letter ...
call osletter7.cmd /targetletter:%osletter% /currentos:%installpath%
)


REM --------------------------------------------------------------------------------------
REM Creating bootmgr menu and bootsect
REM --------------------------------------------------------------------------------------
echo    Creating bootmgr menu and bootsect...
bcdboot %installpath%\Windows /s %bootpath% /l %lang% 2>nul
if exist %bootpath%\ntldr (
REM Post-process tag if exist NT 5.x in order to add an ntldr item in the bootmgr menu
md %installpath%\$ntldr
)
bootsect.exe /nt60 %bootpath% >nul
echo    The boot menu and bootsector has been created! 


REM --------------------------------------------------------------------------------------
REM The autorun script during oobe
REM --------------------------------------------------------------------------------------
if not exist %installpath%\WINDOWS\Setup\Scripts (
md %installpath%\WINDOWS\Setup\Scripts>NUL
)
if exist %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd (
type %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd >>SetupComplete.cmd
)
copy /y SetupComplete.cmd %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd >NUL
echo.

REM --------------------------------------------------------------------------------------
REM copy the $oem$ directory
REM --------------------------------------------------------------------------------------
if exist "%Wimdir%\$OEM$" (
xcopy /? >nul 2>nul
if errorlevel 1 (
echo    xcopy is missing, cannot copy the $oem$ directory.
echo    but the install can continue, press any key... 
echo.
pause >nul
goto end
)
echo    copying %Wimdir%\$OEM$ directory...
xcopy /shey "%Wimdir%\$OEM$\$1" %installpath% >NUL 2>NUL
xcopy /shey "%Wimdir%\$OEM$\$$" %installpath%\WINDOWS >NUL 2>NUL
)

REM --------------------------------------------------------------------------------------
REM end
REM --------------------------------------------------------------------------------------
:end
echo    Everything is completed, press any key to exit.
echo    Please reboot your computer to continue the installation!
Pause>nul
echo on
exit
    
:fail
echo    Press any key to exit ...
Pause>nul
echo on
exit
