@echo off
SET TP=%~dp0
SET TP=%TP:~0,-1%
cd /d "%TP%"
::color 2f
chcp 437
::mode con lines=40 cols=100
set title=                               Windows Vista/2008/7/2008 R2 Fast Installer
set updatetime=   ================================ JTB FIX 2018.02.05 ===========================================
set updatetime=   ================================based on 2010.02.19 ===========================================


:: --------------------------------------------------------------------------------------
:: checking necessary files
:: --------------------------------------------------------------------------------------
for %%i in (imagex bcdboot bootsect reg) do (
	if not exist %%i.exe if not exist %SystemRoot%\system32\%%i.exe (
		echo %%i.exe is missing. Setup can't continue.
		echo.
		echo get imagex.exe from WAIK ^(you can use GetWaikTools.exe^)
		echo get bcdboot.exe, bootsect.exe and reg.exe from a Vista ^(or later^) iso
		echo   from \Windows\System32\ inside the boot.wim or install.wim
		goto fail
	)
)


:: --------------------------------------------------------------------------------------
:: start
:: --------------------------------------------------------------------------------------
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    This installer installs Windows Vista (or later) x86/x64 DIRECTLY to a partition,
echo    without having to boot from a separate DVD or USB stick.
echo    It also creates a boot menu and modifies the bootsector. After it's done, you can reboot
echo    to complete the setup.
echo.
echo    You can directly install Windows to a USB hard disk.
echo.
echo.


reg.exe query "HKU\S-1-5-19" >nul 2>&1 || (
	echo      -------
	echo  *** WARNING ***
	echo      -------
	echo.
	echo.
	echo ADMINISTRATOR PRIVILEGES NOT DETECTED
	echo ____________________________________________________________________________
	echo.
	echo This script require administrator privileges.
	echo.
	echo To do so, right click on this script and select 'Run as administrator'
	echo.
	echo.
	echo.
	echo Press any key to exit...
	pause >nul
	goto :eof
)

echo    Press Enter to continue, or input any letter to show the readme.
echo    If you want to exit, close this window....
SET Choice=
SET /P Choice=
IF "%Choice%" NEQ "" (
	cls
	type readme.txt
	echo.
	echo    Press any key to continue ...
	pause>nul
	cls
)
echo.
echo.
echo.


:: --------------------------------------------------------------------------------------
:: select Windows ISO or install.wim
:: --------------------------------------------------------------------------------------
echo First, select a Windows ISO.
echo if the Windows disc is inserted, mounted or extracted, you can also select the
echo install.wim typically in the "sources" directory of your Windows disc.
echo    Press any key to open file picker...
pause>nul

for /f "delims=" %%I in ('powershell -noprofile .\select-file.ps1') do (
    set wimpath=%%~I
)
echo.

if "%wimpath%"=="" goto fail
if not exist "%wimpath%" (
	echo.
	echo    No ISO or wim file is selected.
	goto fail
)

:: Get the path of the install.wim and its parent directory
if "%wimpath:~-4%" EQU ".iso" (
	echo mounting ISO...
	for /f "delims=" %%I in ('powershell -noprofile .\mount-iso.ps1 "%wimpath%"') do (
		set isodir=%%~I
	)
	set wimdir=%isodir%:\sources
	set wimpath=%wimdir%\install.wim
	if not exist "%wimpath%" (
		echo.
		echo    No ISO or wim file is selected.

		goto fail
	)
) else if "%wimpath:~-4%" EQU ".wim" (
	:: Get the path of the directory which contains install.wim
	for /f "delims=" %%a in ("%wimpath%") do set wimdir=%%~dpa
	set wimdir=%wimdir:~0,-1%
) else (
	echo Please make sure that the file ends with ".iso" or ".wim"
	goto fail
)



:: --------------------------------------------------------------------------------------
:: show the contents of the selected wim file
:: --------------------------------------------------------------------------------------
cd /d "%TP%"
SET Wimindex=
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    Path of your Wim file: %wimpath%
echo.
imagex /info "%wimpath%" >wiminfo.txt


set imageCount=0
for /f "tokens=1,2 delims=:" %%i in (wiminfo.txt) do IF /I "%%i"=="Image Count" set imageCount=%%j
if "%imageCount%" == "0" (
	echo no images found!
	goto fail
)
echo Total image number: %imageCount%
echo.
echo Available image:
echo.
for /f "tokens=2,3 delims=<>=" %%i in (wiminfo.txt) do (
IF /I "%%i"=="IMAGE INDEX" set/p=%%j<nul
IF /I "%%i"=="NAME" echo . %%j
)
echo.

:: --------------------------------------------------------------------------------------
:: select the image number of the wim file
:: --------------------------------------------------------------------------------------
:indexnumber
SET Choice=
SET /P Choice=   Please input the index number (don't include quotation marks):
IF "%Choice%"=="" (
goto indexnumber
) else (
SET Choice=%Choice:~0,3%
)
IF "%Choice%"=="" goto indexnumber
SET errorindex=YES
for /f "tokens=2,3 delims=<>=" %%i in (wiminfo.txt) do IF /I "%%i"=="IMAGE INDEX" IF /I "%%j"==""%Choice%"" SET errorindex=NO
IF '%errorindex%'=='YES' (
echo    The index number is wrong, please input a correct number.
goto indexnumber
)
SET Wimindex=%Choice%
del wiminfo.txt >nul

:: Set language. Set the tag for vista/2008, in order to fix the system drive letter in the registry of Vista/2008.
set isvista=0
set lang=en-US
for /f "tokens=2,3 delims=<>=" %%i in ('imagex /info %wimpath% %Choice%') do (
IF /I "%%i"=="LANGUAGE" set lang=%%j
IF /I "%%i"=="MAJOR" IF /I "%%j" NEQ "6" goto osdrive
IF /I "%%i"=="MINOR" IF /I "%%j"=="0" SET isvista=1
)


:: --------------------------------------------------------------------------------------
:: select system partition
:: --------------------------------------------------------------------------------------
:osdrive
cd /d "%TP%"
SET installpath=
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    Path of your Wim file: %wimpath%
echo    The index number: %Wimindex%
echo.
echo.
echo    Please enter the drive letter of the partition
echo    where you want to install Windows:
echo.
SET Choice=
SET /P Choice=   Please enter the drive letter (e.g: C):
echo.
IF "%Choice%"=="" goto osdrive
SET Choice=%Choice:~0,1%:
vol %Choice%>nul 2>nul || (
echo    The partition %Choice% does not exist, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
echo Testing if the partition is writable >%Choice%\test.tst
if not exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
del %Choice%\test.tst>nul 2>nul
if exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto osdrive
)
SET installpath=%Choice%


:: --------------------------------------------------------------------------------------
:: Verify if the partition is NTFS
:: --------------------------------------------------------------------------------------
md %installpath%\$mft >NUL
if not errorlevel 1 (
rd %installpath%\$mft >NUL
echo.
echo    %installpath% is not NTFS
echo    Please mannually convert or format this partition to NTFS first.
echo.
goto fail
)
cls

:: --------------------------------------------------------------------------------------
:: Verify the free size of the partition
:: --------------------------------------------------------------------------------------
set drvsize=
md %installpath%\$$$ >NUL
for /f "tokens=3" %%i in ('dir /-c %installpath%\') do set drvsize=%%i
rd %installpath%\$$$ >NUL
if %drvsize% LSS 1000000000 goto not_enough
set drvsize=%drvsize:~0,-9%
if %drvsize% LSS 10 (
goto not_enough
) else (
goto enough
)

:not_enough
echo.
echo    The free size of %installpath% is less than 10GB, error may happen if you continue to install.
echo.
SET Choice=
SET /P Choice=    Input C to continue, or input others to select other partition:
IF /I not "%Choice%"=="C" (
goto osdrive
) else (
goto enough
)


:: --------------------------------------------------------------------------------------
:: Processing format
:: --------------------------------------------------------------------------------------
:enough
IF exist %installpath%\windows (
echo.
echo    %installpath% already contains another windows, format is needed.
echo.
) else (
goto bootdrive
)

:format_hd
format /? >nul 2>nul
if errorlevel 1 (
echo    The command "format" is missing in your current OS, installation process cannot continue.
echo    Please format %installpath% by yourself, and then rerun this installer.
goto fail
)
format /FS:NTFS /q /x %installpath%


:: --------------------------------------------------------------------------------------
:: select the boot partition
:: --------------------------------------------------------------------------------------
:bootdrive
cd /d "%TP%"
SET bootpath=
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    Path of your Wim file: %wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo.
echo.
echo    Please enter the drive letter of your ACTIVE primary partition, normally it is C.
echo    If there is an exception to you, please enter the correct letter.
echo    For USB hard drive, the letter is the boot partition of your USB hard drive.
echo.
SET Choice=
SET /P Choice=   Please enter the drive letter (e.g. C):
echo.
IF "%Choice%"=="" goto bootdrive
SET Choice=%Choice:~0,1%:
vol %Choice%>nul 2>nul || (
echo    The partition %Choice% does not exist, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
echo Testing if the partition is writable >%Choice%\test.tst
if not exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
del %Choice%\test.tst>nul 2>nul
if exist %Choice%\test.tst (
echo    Partition %Choice% is not writable, please input again.
echo    Press any key to continue...
pause>nul
goto bootdrive
)
SET bootpath=%Choice%



:: --------------------------------------------------------------------------------------
:: Ask whether the target drive is a USB hard disk
:: --------------------------------------------------------------------------------------
:usbchoice
SET usbhd=
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    Path of your Wim file: %wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo    Drive letter for boot partition: %bootpath%
echo.
echo.
echo.
SET Choice=
SET /P Choice=    Is %installpath% a USB hard disk? (Y/N):
echo.
IF /I NOT "%Choice%"=="Y" IF /I NOT "%Choice%"=="N" goto usbchoice
IF /I "%Choice%"=="Y" (
SET usbhd=YES
) ELSE (
SET usbhd=NO
)


:: --------------------------------------------------------------------------------------
:: Ready to start the installation
:: --------------------------------------------------------------------------------------
cd /d "%TP%"
cls
echo.
echo %title%
echo.
echo %updatetime%
echo.
echo    Path of your Wim file: %wimpath%
echo    The index number: %Wimindex%
echo    Drive letter for OS partition: %installpath%
echo    Drive letter for boot partition: %bootpath%
echo    USB hard disk or not: %usbhd%
echo.
echo.
echo    All informatition is collected! Press Enter to start installation ...
IF %isvista% EQU 1 (
SET osletter=
pause >nul
goto :startinstall
)
echo    After the installation, the drive letter of the OS partition will be C:. If you want to
echo    use other drive letter, please directly input the letter (e.g. D) and press Enter.
echo    Attention! A and B is invalid.

:: --------------------------------------------------------------------------------------
:: get which letter the OS partition will occupy after the installation
:: --------------------------------------------------------------------------------------
:startletter
SET osletter=
SET Choice=
SET /P Choice=
IF "%Choice%"=="" goto startinstall
SET Choice=%Choice:~0,1%
IF /I %Choice% LEQ z IF /I %Choice% GEQ c (
set osletter=%Choice%
) ELSE (
echo    Input error, please reinput the letter or press Enter to start the intallation directly.
goto startletter
)

:: --------------------------------------------------------------------------------------
:: extract the wim file
:: --------------------------------------------------------------------------------------
:startinstall
echo.
echo    Extracting the WIM file, it needs some time, please wait...
imagex /apply %wimpath% %Wimindex% %installpath%
echo    The Wim file applied O.K.
echo.

:: --------------------------------------------------------------------------------------
:: Processing usb boot
:: --------------------------------------------------------------------------------------
cd /d "%TP%"
IF "%usbhd%"=="YES" (
echo.
echo    Processing the registry and usb boot watcher ...
echo.
reg load HKLM\sys %installpath%\WINDOWS\system32\config\system >nul 2>nul
if errorlevel 1 (
echo    Error occured when loading the registry
goto fail
)
REG IMPORT usb.reg >nul 2>nul
if errorlevel 1 (
echo    Error occured when modifying the registry
reg unload HKLM\sys >nul 2>nul
goto fail
)
echo.
reg unload HKLM\sys >nul 2>nul
if errorlevel 1 (
echo    Error occured when unloading the registry
goto fail
)
echo    Finish processing the registry
echo.
copy /y UsbBootWatcher.conf %installpath%\WINDOWS\system32 >NUL
if exist %installpath%\WINDOWS\SysWOW64 (
copy /y UsbBootWatcherx64.exe %installpath%\WINDOWS\system32\UsbBootWatcher.exe >NUL
) ELSE (
copy /y UsbBootWatcherx86.exe %installpath%\WINDOWS\system32\UsbBootWatcher.exe >NUL
)
echo.
:: Post-process tag for USB boot
md %installpath%\$usb
)

:: --------------------------------------------------------------------------------------
:: fix the drive letter of OS partition in the registry of Vista/2008
:: --------------------------------------------------------------------------------------
IF %isvista% EQU 1 (
echo    fixing the drive letter in the registry of Vista/2008
call fixletter.cmd /currentos:%installpath%
)


:: --------------------------------------------------------------------------------------
:: modify the OS partition letter
:: --------------------------------------------------------------------------------------
IF /I '%osletter%' NEQ '' (
echo    Processing the modification of the system drive letter ...
call osletter7.cmd /targetletter:%osletter% /currentos:%installpath%
)


:: --------------------------------------------------------------------------------------
:: Creating bootmgr menu and bootsect
:: --------------------------------------------------------------------------------------
echo    Creating bootmgr menu and bootsect...
bcdboot %installpath%\Windows /s %bootpath% /l %lang% 2>nul
if exist %bootpath%\ntldr (
:: Post-process tag if exist NT 5.x in order to add an ntldr item in the bootmgr menu
md %installpath%\$ntldr
)
bootsect.exe /nt60 %bootpath% >nul
echo    The boot menu and bootsector has been created!


:: --------------------------------------------------------------------------------------
:: The autorun script during oobe
:: --------------------------------------------------------------------------------------
if not exist %installpath%\WINDOWS\Setup\Scripts (
md %installpath%\WINDOWS\Setup\Scripts>NUL
)
if exist %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd (
type %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd >>SetupComplete.cmd
)
copy /y SetupComplete.cmd %installpath%\WINDOWS\Setup\Scripts\SetupComplete.cmd >NUL
echo.

:: --------------------------------------------------------------------------------------
:: copy the $oem$ directory
:: --------------------------------------------------------------------------------------
if exist "%Wimdir%\$OEM$" (
xcopy /? >nul 2>nul
if errorlevel 1 (
echo    xcopy is missing, cannot copy the $oem$ directory.
echo    but the install can continue, press any key...
echo.
pause >nul
goto end
)
echo    copying %Wimdir%\$OEM$ directory...
xcopy /shey "%Wimdir%\$OEM$\$1" %installpath% >NUL 2>NUL
xcopy /shey "%Wimdir%\$OEM$\$$" %installpath%\WINDOWS >NUL 2>NUL
)

:: --------------------------------------------------------------------------------------
:: end
:: --------------------------------------------------------------------------------------
:end
echo    Everything is completed, press any key to exit.
echo    Please reboot your computer to continue the installation!
echo.
if "%isodir%" NEQ "" powershell -noprofile Dismount-DiskImage -DevicePath "\\.\%isodir%:"
Pause>nul
echo on

exit

:fail
echo.
if "%isodir%" NEQ "" powershell -noprofile Dismount-DiskImage -DevicePath "\\.\%isodir%:"
echo    Press any key to exit ...
Pause>nul
echo on
exit
