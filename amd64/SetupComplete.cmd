@echo off



if exist %SystemDrive%\$usb (
powercfg -h off
rd %SystemDrive%\$usb >NUL
)
if exist %SystemDrive%\$ntldr (
bcdedit /delete {ntldr} /f >NUL
bcdedit /create {ntldr} -d "boot.ini Menu" >NUL
bcdedit /set {ntldr} device boot >NUL
bcdedit /set {ntldr} path \ntldr >NUL
bcdedit /displayorder {ntldr} /addlast >NUL
rd %SystemDrive%\$ntldr >NUL
)
