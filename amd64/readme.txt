                    Readme for Windows Vista/2008/7/2008 R2 Fast Installer
===============================================================================================

This installer has 4 advantages compared to the original setup.exe:
1. This installer supports install vista and win7 in the xp based winpe.
2. You can install the OS directly onto a USB external hard drive.
3. You can choose which partition to put the bootmgr and bcd manually, which can prevent putting
   the boot files onto usb disks when booting winpe from usb.
4. If you install win7/2008 with setup.exe from winpe, the OS partition will occupy C: no matter
   which partition you choose. With this installer, you can decide which letter to be used for
   the OS partition in Windows. 


Attention:
1. If you install vista or 2008 with this installer, the windows partition will occupy d:.
2. After installing the OS on a USB hard drive, you are recommended to open the write cache of
   the hard drive in device manager.
3. If you want to plug the usb OS to another PC, please run 
   \Windows\System32\sysprep\sysprep.exe /oobe /generalize /shutdown
   After the shutting down the computer, you can plug the hard disk to another computer.
   
===============================================================================================
